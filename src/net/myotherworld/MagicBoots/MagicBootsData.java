package net.myotherworld.MagicBoots;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class MagicBootsData 
{
	private YamlConfiguration config;
	private File configFile;
	private MagicBoots plugin;
	
	public List<String> enabledWorlds = new ArrayList<String>();
	public String Permissions;
	public String Prefix;
	public String MagicBlocksOn;
	public String MagicBlocksOff;
	public String IP;
	public String Buy;
	public boolean Info;
	public String WWW;
	
	public MagicBootsData(MagicBoots pl)
	{
		this.plugin = pl;
		
		reloadData();
	}
	
	public void reloadData()
	{
		load();
		this.enabledWorlds.clear();
		this.enabledWorlds = this.config.getStringList("config.worlds");	
		
		this.Prefix = ChatColor.translateAlternateColorCodes('&',this.config.getString("Prefix", "MagicBoots"));	
		
		this.Permissions = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Permissions", "You do not have permission!" ));
			
		this.Info = this.config.getBoolean("Premium", true);
				
		this.IP = ChatColor.translateAlternateColorCodes('&',this.config.getString("IP", "Prison.MyOtherWorld.NET"));
		this.Buy = ChatColor.translateAlternateColorCodes('&',this.config.getString("buy", "Plugin do kupienia przez PayPal"));
		this.WWW = ChatColor.translateAlternateColorCodes('&',this.config.getString("WWW", "Shop.MyOtherWorld.NET"));
	}
	
	public void load()
	{
		try
		{
			if (!this.plugin.getDataFolder().exists()) 
			{
				this.plugin.getDataFolder().mkdirs();
			}
			if (!new File(this.plugin.getDataFolder(), "config.yml").exists()) 
			{
				this.plugin.saveResource("config.yml", false);
			}
			this.configFile = new File(this.plugin.getDataFolder(), "config.yml");
			this.config = YamlConfiguration.loadConfiguration(this.configFile);
		}
		catch (Exception ex)
		{
			this.plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
			return;
		}
	}
	public void save(String patch, Object value)
	{
		this.config.set(patch, value);
		try
		{
			this.config.save(this.configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
}

