package net.myotherworld.MagicBoots;


import java.util.HashMap;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import de.slikey.effectlib.effect.AnimatedBallEffect;
import de.slikey.effectlib.effect.AtomEffect;
import de.slikey.effectlib.effect.BleedEffect;
import de.slikey.effectlib.effect.CircleEffect;
import de.slikey.effectlib.effect.CloudEffect;
import de.slikey.effectlib.effect.ConeEffect;
import de.slikey.effectlib.effect.CubeEffect;
import de.slikey.effectlib.effect.DiscoBallEffect;
import de.slikey.effectlib.effect.DnaEffect;
import de.slikey.effectlib.effect.DonutEffect;
import de.slikey.effectlib.effect.DragonEffect;
import de.slikey.effectlib.effect.EarthEffect;
import de.slikey.effectlib.effect.ExplodeEffect;
import de.slikey.effectlib.effect.FlameEffect;
import de.slikey.effectlib.effect.FountainEffect;
import de.slikey.effectlib.effect.GridEffect;
import de.slikey.effectlib.effect.HeartEffect;
import de.slikey.effectlib.effect.HelixEffect;
import de.slikey.effectlib.effect.HillEffect;
import de.slikey.effectlib.effect.IconEffect;
import de.slikey.effectlib.effect.LoveEffect;
import de.slikey.effectlib.effect.MusicEffect;
import de.slikey.effectlib.effect.ShieldEffect;
import de.slikey.effectlib.effect.SmokeEffect;
import de.slikey.effectlib.effect.SphereEffect;
import de.slikey.effectlib.effect.StarEffect;
import de.slikey.effectlib.effect.TextEffect;
import de.slikey.effectlib.effect.TornadoEffect;
import de.slikey.effectlib.effect.TraceEffect;
import de.slikey.effectlib.effect.VortexEffect;
import de.slikey.effectlib.effect.WarpEffect;
import de.slikey.effectlib.effect.WaveEffect;
import de.slikey.effectlib.util.ParticleEffect;

public class EffectListener implements Listener
{
	private HashMap<String, Long> CD = new HashMap<String, Long>();
	
	@EventHandler
	public void Move(PlayerMoveEvent e)
	{
		Player p = e.getPlayer();
	    if (MagicBoots.data.enabledWorlds.contains(p.getWorld().getName()))
	    {	
			
		    if (p.isSprinting())
		    {
				Location loc = p.getLocation();
				ItemStack boots = p.getInventory().getBoots();
					
				if(boots != null && boots.getType() == Material.LEATHER_BOOTS)
				{
					LeatherArmorMeta lam = (LeatherArmorMeta) boots.getItemMeta();				
		    		Color color = lam.getColor();    	
		    		
			    	if(color.equals(Color.fromRGB(0xFF42FC)) && (p.hasPermission("MyOtherWorldMagicBoots.AnimatedBallEffect")))	
			    	{
			    		AnimatedBallEffect EL = new AnimatedBallEffect(MagicBoots.em);		    		
			    		EL.setLocation(loc);	
			    		EL.iterations = 2 * 5;
			    		EL.start();	
			    		return;    		
			    	}
			    	if(color.equals(Color.fromRGB(0xB85933))  && (p.hasPermission("MyOtherWorldMagicBoots.AtomEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		AtomEffect  EL = new AtomEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 3.0D); 	    	
			    		EL.setLocation(loc);
			    		EL.iterations = 4 * 4;
			    		EL.start();	  
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x993333))  && (p.hasPermission("MyOtherWorldMagicBoots.BleedEffect")))	
			    	{
			    		BleedEffect EL = new BleedEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	  
			    		return;
			    	}		 
			    	if(color.equals(Color.fromRGB(0x6699D8))  && (p.hasPermission("MyOtherWorldMagicBoots.CircleEffect")))	
			    	{
			    		CircleEffect EL = new CircleEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 2.0D); 
			    		EL.setLocation(loc);
			    		EL.iterations = 5 * 5;
			    		EL.start();	    
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xFFFFFF))  && (p.hasPermission("MyOtherWorldMagicBoots.CloudEffect")))	
			    	{
			    		CloudEffect EL = new CloudEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.cloudSize= 0.1F;
			    		EL.iterations = 2 * 2;
			    		EL.start();	    		
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}	    	
			    	if(color.equals(Color.fromRGB(0xFF9710))  && (p.hasPermission("MyOtherWorldMagicBoots.ConeEffect")))	
			    	{
			    		ConeEffect EL = new ConeEffect(MagicBoots.em);
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		loc.setY(loc.getY() + 2.0D); 
			    		EL.setLocation(loc);	
			    		EL.iterations = 2 * 10;
			    		EL.start();	  
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	
			    	if(color.equals(Color.fromRGB(0x8C8C8C))  && (p.hasPermission("MyOtherWorldMagicBoots.CubeEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 30)
			    			{
			    				return;
			    			}
			    		}
			    		CubeEffect EL = new CubeEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 3.0D);
			    		EL.setLocation(loc);			    		
			    		EL.start();	    
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	
			    	if(color.equals(Color.fromRGB(0xDBAC99))  && (p.hasPermission("MyOtherWorldMagicBoots.DiscoBallEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 6)
			    			{
			    				return;
			    			}
			    		}
			    		DiscoBallEffect EL = new DiscoBallEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 10.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 10;
			    		EL.start();	    	
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xE5E533))  && (p.hasPermission("MyOtherWorldMagicBoots.DnaEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 4)
			    			{
			    				return;
			    			}
			    		}
			    		DnaEffect EL = new DnaEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 2.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 5 * 10;
			    		EL.start();	    		
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xF8F8CC))  && (p.hasPermission("MyOtherWorldMagicBoots.DonutEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		DonutEffect EL = new DonutEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 5.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	 
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xF2F299))  && (p.hasPermission("MyOtherWorldMagicBoots.DragonEffect")))	
			    	{    		
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		DragonEffect EL = new DragonEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 2.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 10;
			    		EL.pitch = 0.2F;
			    		EL.start();	  
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xB2BF99))  && (p.hasPermission("MyOtherWorldMagicBoots.EarthEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		EarthEffect EL = new EarthEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 4.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xCC9999))  && (p.hasPermission("MyOtherWorldMagicBoots.ExplodeEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		ExplodeEffect EL = new ExplodeEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 4.0D);
			    		EL.setLocation(loc);
			    		EL.amount = 2;
			    		EL.iterations = 1 * 1;
			    		EL.start();	    	
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xEBBF99))  && (p.hasPermission("MyOtherWorldMagicBoots.FlameEffect")))	
			    	{
			    		FlameEffect EL = new FlameEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xFBFBE5))  && (p.hasPermission("MyOtherWorldMagicBoots.FountainEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		FountainEffect EL = new FountainEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	 
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x9F3F3F))  && (p.hasPermission("MyOtherWorldMagicBoots.GridEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		GridEffect EL = new GridEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 2.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 2 * 5;
			    		EL.start();
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x99A5D8))  && (p.hasPermission("MyOtherWorldMagicBoots.HeartEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 1)
			    			{
			    				return;
			    			}
			    		}
			    		HeartEffect EL = new HeartEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 2.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 2 * 10;
			    		EL.start();	 
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xD3A299))  && (p.hasPermission("MyOtherWorldMagicBoots.HelixEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		HelixEffect EL = new HelixEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 3.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 2 * 1;
			    		EL.start();	    
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xF05C0F))  && (p.hasPermission("MyOtherWorldMagicBoots.HillEffect")))	
			    	{
			    		HillEffect EL = new HillEffect(MagicBoots.em);
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    	
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}	
			    	if(color.equals(Color.fromRGB(0xB1A5AC))  && (p.hasPermission("MyOtherWorldMagicBoots.IconEffect")))	
			    	{
			    		IconEffect EL = new IconEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    
			    		return;
			    	}
	
			    	if(color.equals(Color.fromRGB(0xF3162A))  && (p.hasPermission("MyOtherWorldMagicBoots.LoveEffect")))	
			    	{
			    		LoveEffect EL = new LoveEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x55B441))  && (p.hasPermission("MyOtherWorldMagicBoots.MusicEffect")))	
			    	{
			    		MusicEffect EL = new MusicEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x30A7BB))  && (p.hasPermission("MyOtherWorldMagicBoots.ShieldEffect")))	
			    	{
			    		ShieldEffect EL = new ShieldEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x2D3739))  && (p.hasPermission("MyOtherWorldMagicBoots.SmokeEffect")))	
			    	{
			    		SmokeEffect EL = new SmokeEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.particle = ParticleEffect.SMOKE_LARGE;
			    		EL.iterations = 2 * 5;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0x3D4648))  && (p.hasPermission("MyOtherWorldMagicBoots.SphereEffect")))	
			    	{
			    		SphereEffect EL = new SphereEffect(MagicBoots.em);
			    		loc.setY(loc.getY() + 1.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    	    	
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xF0CB3D))  && (p.hasPermission("MyOtherWorldMagicBoots.StarEffect")))	
			    	{
			    		StarEffect EL = new StarEffect(MagicBoots.em);
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		loc.setY(loc.getY() + 5.0D);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xFBF9EE))  && (p.hasPermission("MyOtherWorldMagicBoots.TextEffect")))	
			    	{		    		
			    		TextEffect EL = new TextEffect(MagicBoots.em);
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		loc.setY(loc.getY() + 3.0D);
			    		EL.setLocation(loc);
			    		EL.particle = ParticleEffect.FLAME;
			    		EL.iterations = 1 * 1;
			    		EL.text = "MyOtherWorld.NET";	
			    		EL.start();
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xDE7212))  && (p.hasPermission("MyOtherWorldMagicBoots.TornadoEffect")))	
			    	{
			    		TornadoEffect EL = new TornadoEffect(MagicBoots.em);
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 3)
			    			{
			    				return;
			    			}
			    		}
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xFFE060))  && (p.hasPermission("MyOtherWorldMagicBoots.TraceEffect")))	
			    	{
			    		TraceEffect EL = new TraceEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 2 * 5;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xE5E7F5))  && (p.hasPermission("MyOtherWorldMagicBoots.WarpEffect")))	
			    	{
			    		WarpEffect EL = new WarpEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    		
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xB8C0F5))  && (p.hasPermission("MyOtherWorldMagicBoots.WaveEffect")))	
			    	{
			    		if (CD.containsKey(p.getName()))
			    		{
			    			long diff = (System.currentTimeMillis() - CD.get(p.getName())) / 1000;
			    			if (diff < 2)
			    			{
			    				return;
			    			}
			    		}
			    		WaveEffect EL = new WaveEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 1 * 1;
			    		EL.start();	    
			    		CD.put(p.getName(), System.currentTimeMillis());
			    		return;
			    	}
			    	if(color.equals(Color.fromRGB(0xD88855))  && (p.hasPermission("MyOtherWorldMagicBoots.VortexEffect")))	
			    	{
			    		VortexEffect EL = new VortexEffect(MagicBoots.em);
			    		EL.setLocation(loc);
			    		EL.iterations = 2 * 2;
			    		EL.start();	    		
			    		return;
			    	}
				}
		    }
				    
		}
	}
}
		    	

