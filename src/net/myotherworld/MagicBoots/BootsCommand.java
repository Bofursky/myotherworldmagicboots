package net.myotherworld.MagicBoots;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BootsCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
		
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }		
		if(sender.hasPermission("MyOtherWorldMagicBoots.menu")) 
		{
			Player p = (Player)sender;
			BootsInventory.loadBootsInv(p);
			return true;
		}
		else
		{
			sender.sendMessage(MagicBoots.data.Prefix + MagicBoots.data.Permissions);
		}
		return true;
	}
}

