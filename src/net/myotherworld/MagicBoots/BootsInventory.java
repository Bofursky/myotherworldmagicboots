package net.myotherworld.MagicBoots;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class BootsInventory 
{
	public static void loadBootsInv(Player p)
	{
		Inventory inv = Bukkit.createInventory(null, 36, "�6Magiczne buty MyOtherWorld");
		
	    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(p.hasPermission("MyOtherWorldMagicBoots.AnimatedBallB"))
	    {
		    ItemStack AnimatedBallB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta AnimatedBallLam = (LeatherArmorMeta)AnimatedBallB.getItemMeta();
		    AnimatedBallLam.setColor(Color.fromRGB(0xFF42FC));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Gwiezdne Trace");
		    AnimatedBallLam.setLore(lore);
		    AnimatedBallB.setItemMeta(AnimatedBallLam);
		    ItemMeta AnimatedBallMeta = AnimatedBallB.getItemMeta();
		    AnimatedBallMeta.setDisplayName("�2AnimatedBallBoots");
		    AnimatedBallB.setItemMeta(AnimatedBallMeta);
		    inv.addItem(AnimatedBallB);
	    }	
	    if(p.hasPermission("MyOtherWorldMagicBoots.AtomEffect"))
	    {	
		    ItemStack AtomB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta AtomLam = (LeatherArmorMeta)AtomB.getItemMeta();
		    AtomLam.setColor(Color.fromRGB(0xB85933));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Widze atomy");
		    AtomLam.setLore(lore);
		    AtomB.setItemMeta(AtomLam);
		    ItemMeta AtomMeta = AtomB.getItemMeta();
		    AtomMeta.setDisplayName("�2AtomBoots");
		    AtomB.setItemMeta(AtomMeta);
		    inv.addItem(AtomB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.BleedEffect"))
	    {
		    ItemStack BleedB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta BleedLam = (LeatherArmorMeta)BleedB.getItemMeta();
		    BleedLam.setColor(Color.fromRGB(0x993333));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Umieram");
		    BleedLam.setLore(lore);
		    BleedB.setItemMeta(BleedLam);
		    ItemMeta BleedMeta = BleedB.getItemMeta();
		    BleedMeta.setDisplayName("�2BleedEffect");
		    BleedB.setItemMeta(BleedMeta);
		    inv.addItem(BleedB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.CircleEffect"))
	    {
		    ItemStack CircleB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta CircleLam = (LeatherArmorMeta)CircleB.getItemMeta();
		    CircleLam.setColor(Color.fromRGB(0x6699D8));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Wirujace gwiazdki");
		    CircleLam.setLore(lore);
		    CircleB.setItemMeta(CircleLam);
		    ItemMeta CircleMeta = CircleB.getItemMeta();
		    CircleMeta.setDisplayName("�2CircleBoots");
		    CircleB.setItemMeta(CircleMeta);
		    inv.addItem(CircleB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.CloudEffect"))
	    {
		    ItemStack CloudB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta CloudLam = (LeatherArmorMeta)CloudB.getItemMeta();
		    CloudLam.setColor(Color.fromRGB(0xFFFFFF));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Burza nadchodzi");
		    CloudLam.setLore(lore);
		    CloudB.setItemMeta(CloudLam);
		    ItemMeta CloudMeta = CloudB.getItemMeta();
		    CloudMeta.setDisplayName("�2CloudBoots");
		    CloudB.setItemMeta(CloudMeta);
		    inv.addItem(CloudB);		   
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.ConeEffect"))
	    {
		    ItemStack ConeB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta ConeLam = (LeatherArmorMeta)ConeB.getItemMeta();
		    ConeLam.setColor(Color.fromRGB(0xFF9710));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Swider");
		    ConeLam.setLore(lore);	
		    ConeB.setItemMeta(ConeLam);
		    ItemMeta ConeMeta = ConeB.getItemMeta();
		    ConeMeta.setDisplayName("�2ConeBoots");
		    ConeB.setItemMeta(ConeMeta);	
		    inv.addItem(ConeB);
	    }
		if(p.hasPermission("MyOtherWorldMagicBoots.CubeEffect"))	    
	    {
		    ItemStack CubeB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta CubeLam = (LeatherArmorMeta)CubeB.getItemMeta();
		    CubeLam.setColor(Color.fromRGB(0x8C8C8C));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Kosci zostaly rzucone");
		    CubeLam.setLore(lore);
		    CubeB.setItemMeta(CubeLam);
		    ItemMeta CubeMeta = CubeB.getItemMeta();
		    CubeMeta.setDisplayName("�2CubeBoots");
		    CubeB.setItemMeta(CubeMeta);
		    inv.addItem(CubeB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.DiscoBallEffect"))
	    {
		    ItemStack DiscoBallB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta DiscoBallLam = (LeatherArmorMeta)DiscoBallB.getItemMeta();
		    DiscoBallLam.setColor(Color.fromRGB(0xDBAC99));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Disco ponad wszystko");
		    DiscoBallLam.setLore(lore);
		    DiscoBallB.setItemMeta(DiscoBallLam);		    
		    ItemMeta DiscoBallMeta = DiscoBallB.getItemMeta();
		    DiscoBallMeta.setDisplayName("�2DiscoBallBoots");
		    DiscoBallB.setItemMeta(DiscoBallMeta);
		    inv.addItem(DiscoBallB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.DnaEffect"))
	    {
		    ItemStack DnaB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta DnaLam = (LeatherArmorMeta)DnaB.getItemMeta();
		    DnaLam.setColor(Color.fromRGB(0xE5E533));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Kwas deoksyrybonukleinowy");
		    DnaLam.setLore(lore);
		    DnaB.setItemMeta(DnaLam);
		    ItemMeta DnaMeta = DnaB.getItemMeta();
		    DnaMeta.setDisplayName("�2DnaBoots");
		    DnaB.setItemMeta(DnaMeta);
		    inv.addItem(DnaB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.DonutEffect"))
	    {
		    ItemStack DonutB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta DonutLam = (LeatherArmorMeta)DonutB.getItemMeta();
		    DonutLam.setColor(Color.fromRGB(0xF8F8CC));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Pamdexowe Ciastko");
		    DonutLam.setLore(lore);
		    DonutB.setItemMeta(DonutLam);
		    ItemMeta DonutMeta = DonutB.getItemMeta();
		    DonutMeta.setDisplayName("�2DonutBoots");
		    DonutB.setItemMeta(DonutMeta);
		    inv.addItem(DonutB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.DragonEffect"))
	    {
		    ItemStack DragonB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta DragonLam = (LeatherArmorMeta)DragonB.getItemMeta();
		    DragonLam.setColor(Color.fromRGB(0xF2F299));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Smoczy ognien");
		    DragonLam.setLore(lore);
		    DragonB.setItemMeta(DragonLam);
		    ItemMeta DragonMeta = DragonB.getItemMeta();
		    DragonMeta.setDisplayName("�2DragonBoots");
		    DragonB.setItemMeta(DragonMeta);
		    inv.addItem(DragonB);
	    }	    
	    if(p.hasPermission("MyOtherWorldMagicBoots.EarthEffect"))
	    {
		    ItemStack EarthB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta EarthLam = (LeatherArmorMeta)EarthB.getItemMeta();
		    EarthLam.setColor(Color.fromRGB(0xB2BF99));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Ziemia");
		    EarthLam.setLore(lore);
		    EarthB.setItemMeta(EarthLam);
		    ItemMeta EarthMeta = EarthB.getItemMeta();
		    EarthMeta.setDisplayName("�2EarthBoots");
		    EarthB.setItemMeta(EarthMeta);
		    inv.addItem(EarthB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.ExplodeEffect"))
	    {
		    ItemStack ExplodeB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta ExplodeLam = (LeatherArmorMeta)ExplodeB.getItemMeta();
		    ExplodeLam.setColor(Color.fromRGB(0xCC9999));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Tssss Bum...");
		    ExplodeLam.setLore(lore);
		    ExplodeB.setItemMeta(ExplodeLam);
		    ItemMeta ExplodeMeta = ExplodeB.getItemMeta();
		    ExplodeMeta.setDisplayName("�2ExplodeBoots");
		    ExplodeB.setItemMeta(ExplodeMeta);
		    inv.addItem(ExplodeB);
	    }   
	    if(p.hasPermission("MyOtherWorldMagicBoots.FlameEffect"))
	    {
		    ItemStack FlameB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta FlameLam = (LeatherArmorMeta)FlameB.getItemMeta();
		    FlameLam.setColor(Color.fromRGB(0xEBBF99));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Pale sie");
		    FlameLam.setLore(lore);
		    FlameB.setItemMeta(FlameLam);
		    ItemMeta FlameMeta = FlameB.getItemMeta();
		    FlameMeta.setDisplayName("�2FlameBoots");
		    FlameB.setItemMeta(FlameMeta);
		    inv.addItem(FlameB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.FountainEffect"))
	    {
		    ItemStack FountainB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta FountainLam = (LeatherArmorMeta)FountainB.getItemMeta();
		    FountainLam.setColor(Color.fromRGB(0xFBFBE5));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Fontanna szczescia");
		    FountainLam.setLore(lore);	    
		    FountainB.setItemMeta(FountainLam);
		    ItemMeta FountainMeta = FountainB.getItemMeta();
		    FountainMeta.setDisplayName("�2FountainBoots");
		    FountainB.setItemMeta(FountainMeta);
		    inv.addItem(FountainB);
	    }	   
	    if(p.hasPermission("MyOtherWorldMagicBoots.GridEffect"))
	    {
		    ItemStack GridB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta GridLam = (LeatherArmorMeta)GridB.getItemMeta();
		    GridLam.setColor(Color.fromRGB(0x9F3F3F));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Ramka");
		    GridLam.setLore(lore);	    
		    GridB.setItemMeta(GridLam);
		    ItemMeta GridMeta = GridB.getItemMeta();
		    GridMeta.setDisplayName("�2GridBoots");
		    GridB.setItemMeta(GridMeta);	   
		    inv.addItem(GridB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.HeartEffect"))
	    {
		    ItemStack HeartB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta HeartLam = (LeatherArmorMeta)HeartB.getItemMeta();
		    HeartLam.setColor(Color.fromRGB(0x99A5D8));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Niebieskie serce");
		    HeartLam.setLore(lore);	
		    HeartB.setItemMeta(HeartLam);
		    ItemMeta HeartMeta = HeartB.getItemMeta();
		    HeartMeta.setDisplayName("�2HeartBoots");
		    HeartB.setItemMeta(HeartMeta);
		    inv.addItem(HeartB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.HelixEffect"))
	    {
		    ItemStack HelixB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta HelixLam = (LeatherArmorMeta)HelixB.getItemMeta();
		    HelixLam.setColor(Color.fromRGB(0xD3A299));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Pierscien ognia");
		    HelixLam.setLore(lore);	
		    HelixB.setItemMeta(HelixLam);
		    ItemMeta HelixMeta = HelixB.getItemMeta();
		    HelixMeta.setDisplayName("�2HelixBoots");
		    HelixB.setItemMeta(HelixMeta);
		    inv.addItem(HelixB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.HillEffect"))
	    {
		    ItemStack HillB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta HillLam = (LeatherArmorMeta)HillB.getItemMeta();
		    HillLam.setColor(Color.fromRGB(0xF05C0F));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Bunkier");
		    HillLam.setLore(lore);	
		    HillB.setItemMeta(HillLam);
		    ItemMeta HillMeta = HillB.getItemMeta();
		    HillMeta.setDisplayName("�2HillBoots");
		    HillB.setItemMeta(HillMeta);
		    inv.addItem(HillB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.IconEffect"))
	    {
		    ItemStack IconB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta IconLam = (LeatherArmorMeta)IconB.getItemMeta();
		    IconLam.setColor(Color.fromRGB(0xB1A5AC));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Pilki");
		    IconLam.setLore(lore);	
		    IconB.setItemMeta(IconLam);
		    ItemMeta IconMeta = IconB.getItemMeta();
		    IconMeta.setDisplayName("�2IconBoots");
		    IconB.setItemMeta(IconMeta);	
		    inv.addItem(IconB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.LoveEffect"))
	    {
		    ItemStack LoveB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta LoveLam = (LeatherArmorMeta)LoveB.getItemMeta();
		    LoveLam.setColor(Color.fromRGB(0xF3162A));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Milosc");
		    LoveLam.setLore(lore);	
		    LoveB.setItemMeta(LoveLam);
		    ItemMeta LoveMeta = LoveB.getItemMeta();
		    LoveMeta.setDisplayName("�2LoveBoots");
		    LoveB.setItemMeta(LoveMeta);	
		    inv.addItem(LoveB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.MusicEffect"))
	    {
		    ItemStack MusicB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta MusicLam = (LeatherArmorMeta)MusicB.getItemMeta();
		    MusicLam.setColor(Color.fromRGB(0x55B441));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Muzyka");
		    MusicLam.setLore(lore);	
		    MusicB.setItemMeta(MusicLam);
		    ItemMeta MusicMeta = MusicB.getItemMeta();
		    MusicMeta.setDisplayName("�2ItemBoots");
		    MusicB.setItemMeta(MusicMeta);	
		    inv.addItem(MusicB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.ShieldEffect"))
	    {
		    ItemStack ShieldB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta ShieldLam = (LeatherArmorMeta)ShieldB.getItemMeta();
		    ShieldLam.setColor(Color.fromRGB(0x30A7BB));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Smocza zbroja");
		    ShieldLam.setLore(lore);	
		    ShieldB.setItemMeta(ShieldLam);
		    ItemMeta ShieldMeta = ShieldB.getItemMeta();
		    ShieldMeta.setDisplayName("�2ShieldBoots");
		    ShieldB.setItemMeta(ShieldMeta);
		    inv.addItem(ShieldB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.SmokeEffect"))
	    {
		    ItemStack SmokeB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta SmokeLam = (LeatherArmorMeta)SmokeB.getItemMeta();
		    SmokeLam.setColor(Color.fromRGB(0x2D3739));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Sciana dymu");
		    SmokeLam.setLore(lore);	
		    SmokeB.setItemMeta(SmokeLam);
		    ItemMeta SmokeMeta = SmokeB.getItemMeta();
		    SmokeMeta.setDisplayName("�2SmokeBoots");
		    SmokeB.setItemMeta(SmokeMeta);	
		    inv.addItem(SmokeB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.SphereEffect"))
	    {
		    ItemStack SphereB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta SphereLam = (LeatherArmorMeta)SphereB.getItemMeta();
		    SphereLam.setColor(Color.fromRGB(0x3D4648));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Kopci sie");
		    SphereLam.setLore(lore);	
		    SphereB.setItemMeta(SphereLam);
		    ItemMeta SphereMeta = SphereB.getItemMeta();
		    SphereMeta.setDisplayName("�2SphereBoots");
		    SphereB.setItemMeta(SphereMeta);	
		    inv.addItem(SphereB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.StarEffect"))
	    {
		    ItemStack StarB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta StarLam = (LeatherArmorMeta)StarB.getItemMeta();
		    StarLam.setColor(Color.fromRGB(0xF0CB3D));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Gwiazda");
		    StarLam.setLore(lore);	
		    StarB.setItemMeta(StarLam);
		    ItemMeta StarMeta = StarB.getItemMeta();
		    StarMeta.setDisplayName("�2StarBoots");
		    StarB.setItemMeta(StarMeta);	
		    inv.addItem(StarB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.TextEffect"))
	    {
		    ItemStack TextB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta TextLam = (LeatherArmorMeta)TextB.getItemMeta();
		    TextLam.setColor(Color.fromRGB(0xFBF9EE));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("MyOtherWorld");
		    TextLam.setLore(lore);	
		    TextB.setItemMeta(TextLam);
		    ItemMeta TextMeta = TextB.getItemMeta();
		    TextMeta.setDisplayName("�2TextBoots");
		    TextB.setItemMeta(TextMeta);	
		    inv.addItem(TextB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.TornadoEffect"))
	    {
		    ItemStack TornadoB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta TornadoLam = (LeatherArmorMeta)TornadoB.getItemMeta();
		    TornadoLam.setColor(Color.fromRGB(0xDE7212));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Duze tornado");
		    TornadoLam.setLore(lore);	
		    TornadoB.setItemMeta(TornadoLam);
		    ItemMeta TornadoMeta = TornadoB.getItemMeta();
		    TornadoMeta.setDisplayName("�2TornadoBoots");
		    TornadoB.setItemMeta(TornadoMeta);
		    inv.addItem(TornadoB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.TraceEffect"))
	    {
		    ItemStack TraceB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta TraceLam = (LeatherArmorMeta)TraceB.getItemMeta();
		    TraceLam.setColor(Color.fromRGB(0xFFE060));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Wezyk");
		    TraceLam.setLore(lore);	   
		    TraceB.setItemMeta(TraceLam);
		    ItemMeta TraceMeta = TraceB.getItemMeta();
		    TraceMeta.setDisplayName("�2TraceBoots");
		    TraceB.setItemMeta(TraceMeta);
		    inv.addItem(TraceB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.WarpEffect"))
	    {
		    ItemStack WarpB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta WarpLam = (LeatherArmorMeta)WarpB.getItemMeta();
		    WarpLam.setColor(Color.fromRGB(0xE5E7F5));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Mleczna droga");
		    WarpLam.setLore(lore);	
		    WarpB.setItemMeta(WarpLam);
		    ItemMeta WarpMeta = WarpB.getItemMeta();
		    WarpMeta.setDisplayName("�2WarpBoots");
		    WarpB.setItemMeta(WarpMeta);
		    inv.addItem(WarpB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.WaveEffect"))
	    {
		    ItemStack WaveB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta WaveLam = (LeatherArmorMeta)WaveB.getItemMeta();
		    WaveLam.setColor(Color.fromRGB(0xB8C0F5));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Fala");
		    WaveLam.setLore(lore);	
		    WaveB.setItemMeta(WaveLam);
		    ItemMeta WaveMeta = WaveB.getItemMeta();
		    WaveMeta.setDisplayName("�2WaveBoots");
		    WaveB.setItemMeta(WaveMeta);
		    inv.addItem(WaveB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.VortexEffect"))
	    {
		    ItemStack VortexB = new ItemStack(Material.LEATHER_BOOTS);
		    LeatherArmorMeta VortexLam = (LeatherArmorMeta)VortexB.getItemMeta();
		    VortexLam.setColor(Color.fromRGB(0xD88855));
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Obrecze");
		    VortexLam.setLore(lore);
		    VortexB.setItemMeta(VortexLam);
		    ItemMeta VortexMeta = VortexB.getItemMeta();
		    VortexMeta.setDisplayName("�2VortexBoots");
		    VortexB.setItemMeta(VortexMeta);
		    inv.addItem(VortexB);
	    }
	    if(p.hasPermission("MyOtherWorldMagicBoots.menu"))
	    {
	    	SkullMeta  meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
	    	meta.setOwner("Skynets");
	    	ItemStack stack = new ItemStack(Material.SKULL_ITEM,1 , (byte)3);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Skynets");
		    lore.add("Premium Version");
		    if(!(MagicBoots.data.Info))
		    {
		    	lore.add(MagicBoots.data.IP);
		    	lore.add(MagicBoots.data.Buy);
		    	lore.add(MagicBoots.data.WWW);
		    }
		    meta.setLore(lore);
	    	meta.setDisplayName("�6Autor MagicBoots");
	    	stack.setItemMeta(meta);	  
		    inv.addItem(stack);
	    }
	    p.openInventory(inv);
	}
}
