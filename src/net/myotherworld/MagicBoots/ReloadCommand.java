package net.myotherworld.MagicBoots;


import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandExecutor 
{
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if(sender.hasPermission("MyOtherWorldMagicBoots.reload")) 
		{		
	        MagicBoots.data.reloadData();
	        sender.sendMessage(MagicBoots.data.Prefix + "Poprawnie przeladowano config.");
	        return true;
		}
		else
		{
			sender.sendMessage(MagicBoots.data.Prefix + MagicBoots.data.Permissions);
		}
		return true;	     
	}
	

}
