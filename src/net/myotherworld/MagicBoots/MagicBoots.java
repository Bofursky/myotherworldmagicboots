package net.myotherworld.MagicBoots;


import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;

public class MagicBoots extends JavaPlugin
{
	
	public static EffectManager em;
	public static MagicBootsData data;
	public final EffectListener el = new EffectListener();
    public final InfoListener il = new InfoListener();
	
	private void listeners()
	{
    	PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new EffectListener(), this);
        pm.registerEvents(new InfoListener(), this);;
	}
	public void onEnable()
	{
		em = new EffectManager(EffectLib.instance());
		data = new MagicBootsData(this);
		listeners();
		
		getCommand("Boots").setExecutor( new BootsCommand() );
		getCommand("BootsReload").setExecutor( new ReloadCommand() );
	}
	   
}
